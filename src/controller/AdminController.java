
 
package controller;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Optional;

import app.Photos;

import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.User;
/**
 * Controller class which handles Admin actions
 * @author Kelvin Liu
 *
 */
public class AdminController extends AbstractController {
	/**
	 * ListView of the Users registered with the app
	 */
	@FXML
	public ListView<User> userList = new ListView<User>();
	@FXML
	Button logoutButton;
	@FXML
	Button addUser;
	@FXML
	Button deleteUser;

//	private Stage s;
	
	// ArrayList<User> users = new ArrayList<User>();
	ObservableList<User> observableUserList = FXCollections.<User>observableArrayList();
	/**
	 * Path containing the userFile
	 */
	public static final Path userDir = Paths.get(Photos.storeDir + Photos.userFile);
	/**
	 * Path containing the directory of individual Users
	 */
	public static final Path individualUserDir = Paths.get(Photos.storeDir + Photos.individualUserDir);
	
	
	/* (non-Javadoc) Starts the AdminController and sets up userMap from file
	 * @see controller.AbstractController#start(javafx.stage.Stage, java.lang.String[])
	 * @param stage Stage to maintain 
	 * @param args 
	 */
	@SuppressWarnings("unchecked")
	public void start(Stage stage, String... args) {
		this.s = stage;
		// System.out.println(System.getProperty("user.dir"));
		ArrayList<User> tempUser = new ArrayList<User>();
		// deserialize here
		try {
			// load userNames, userList
			File yourFile = new File(userDir.toString());
			yourFile.createNewFile(); // if file already exists will do nothing

			FileInputStream fileIn = new FileInputStream(yourFile);
			if (fileIn.available() > 0) {
				ObjectInputStream in = new ObjectInputStream(fileIn);
				tempUser = (ArrayList<User>) in.readObject();
				if (tempUser == null) {
					System.out.println("why is this null");
				}
				in.close();
			}
			observableUserList = FXCollections.<User>observableList(tempUser);
//			System.out.println(observableUserList.get(0).toString());
			userList.setItems(observableUserList);
			fileIn.close();

			// load userMap
			yourFile = new File(Photos.userMapDir.toString());
			yourFile.createNewFile(); // if file already exists will do nothing

			fileIn = new FileInputStream(yourFile);
			if (fileIn.available() > 0) {
				ObjectInputStream in = new ObjectInputStream(fileIn);
				Photos.userMap = (HashMap<String, Boolean>) in.readObject();
				in.close();
			}
			if (Photos.userMap == null)
				Photos.userMap = new HashMap<String, Boolean>();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			System.out.println("This class not found1");
			c.printStackTrace();
			return;
		}
	}

	/**
	 * Adds a user to the userMap	
	 * @param ev ActionEvent to trigger the method
	 */
	public void addUser(ActionEvent ev) {

		// try {
		while (true) {
			// display prompt and use that to create another
			TextInputDialog dialog = new TextInputDialog("");
			dialog.setTitle("Enter New Username");
			dialog.setHeaderText("Create New User");
			Optional<String> result = dialog.showAndWait(); // get input
			if (result.isPresent()) { // if pressed OK
				if (result.get().isEmpty()) { // check if name isnt empty string
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("");
					alert.setHeaderText("Error");
					alert.setContentText("Username is empty");
					alert.showAndWait();
					continue;
				}
				if (Photos.userMap.containsKey(result.get())) { // check if name already exists
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText("Username Already Exists");
					alert.showAndWait();
					continue;
				}
				// correct input
				User u = new User(result.get());
				observableUserList.add(u);
				Photos.userMap.put(result.get(), true);
				try {// add new individual user.dat
					FileOutputStream fos = new FileOutputStream(individualUserDir+File.separator+result.get()+".dat");
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(u);
					oos.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			} else {
				break;
			}
		}
	}

	/**
	 * Deletes the currently selected User from the userMap
	 * @param ev ActionEvent to trigger the method
	 */
	public void deleteUser(ActionEvent ev) {
		if (userList.getSelectionModel().getSelectedItem() == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("No User Selected");
			alert.setContentText("");
			alert.showAndWait();
			return;
		}
		try {
			// display the confirmation prompt
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirm Delete");
			alert.setHeaderText("Delete User?");
			// alert.setContentText("Ooops, there was an error!");
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				User u = userList.getSelectionModel().getSelectedItem();
				observableUserList.remove(u);
				Photos.userMap.remove(u.getName());
				// remove individual user.dat
				File file = new File(individualUserDir + File.separator + u.getName()+".dat");
				System.out.println(file);
				if (!file.delete()) {
					System.out.println("NOT DELETED SUCCESSFULLY");
				}
			} else {
				return;
			}
		} catch (Exception e) {
			showError(e);
			return;
		}
	}

	/**
	 * Logs the Admin out of the Admin session
	 * @param ev ActionEvent to trigger this method
	 */
	public void logout(ActionEvent ev) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Logout");
		alert.setHeaderText("Logout?");
		alert.setContentText("Changes will be saved");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			this.write();
			// go back to login screen
			LoginController a = new LoginController();
			String fxml = "../view/LoginPage.fxml";
			switchScene(this, a, this.s, fxml);
		}
	}

	/**
	 * Serialize userNames by converting it to a List
	 * 
	 */
	public void write() {
//		System.out.println("Try Serializing");
		try {
			// write userList to file
			FileOutputStream fos = new FileOutputStream(userDir.toString());
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(new ArrayList<User>(observableUserList));
			oos.close();

			// write userMap to a file
			fos = new FileOutputStream(Photos.userMapDir.toString());
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Photos.userMap);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Displays a confirmation and writes session data to file
	 * @return boolean determining if exit was successful or not
	 */
	public boolean onExit() {
		this.write();
		Optional<ButtonType> result = AbstractController.showConfirmation("Exit?", "Changes have been saved.");
		if(result.get() == ButtonType.OK) {
			return true;
		}
		else {
			return false;
		}
	}

	
}
