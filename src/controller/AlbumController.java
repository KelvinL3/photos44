
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.*;

/**
 * Controller class to handle Albums for a particular User
 * @author Kelvin Liu
 */
public class AlbumController extends AbstractController {

	@FXML
	Button logoutButton;
	@FXML
	Button addAlbum;
	@FXML
	Button deleteAlbum;
	@FXML
	Button renameAlbum;
	@FXML
	TextField search;
	@FXML
	Button searchButton;
	@FXML
	ListView<Album> albumList = new ListView<Album>();


	/**
	 * ObservableList of Albums 
	 */
	public ObservableList<Album> observableAlbumList;

	/**
	 * Prevents multiple instances of the same album
	 */
	public HashMap<String, Boolean> albumMap;

	// for switching stages / scenes
	// private Stage s;
	
	
	User user;

	// for synch
	ArrayList<User> tempUser = null;
	User syncUser = null;

	/**
	 * Path corresponding to individual User data
	 */
	public static final Path userLoadDir = Paths.get(Photos.storeDir + Photos.individualUserDir + File.separator);

	/* (non-Javadoc) Starts the AlbumController and sets up the list of Albums
	 * @see controller.AbstractController#start(javafx.stage.Stage, java.lang.String[])
	 * @param stage Stage to maintain
	 * @param args corresponds to the active User
	 */
	@Override
	public void start(Stage stage, String... args) throws Exception {
		this.s = stage;
		this.read(args);
		observableAlbumList = FXCollections.<Album>observableArrayList(user.getAlbums());
		
		albumList.setItems(observableAlbumList);
		if (!observableAlbumList.isEmpty()) {
			albumList.getSelectionModel().select(0);
		}
		
		albumList.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    @Override
		    public void handle(MouseEvent click) {
		        if (click.getClickCount() == 2) {
		        	Album rowData = albumList.getSelectionModel().getSelectedItem();
		        	openAlbum(rowData);
		        }
		    }
		});
		
	}

	/**
	 * Reads the 3 files associated with the User
	 * @param args the username of the User
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "resource" })
	public void read(String... args) throws Exception {
//		System.out.println("\t Sign in as " + args[0]);
		if (args[0] == null)
			try {
				throw new Exception("Must instantiate username");
			} catch (Exception e) {
				e.printStackTrace();
			}
		try {
			// for user (User)
			File userFile = new File(userLoadDir + File.separator + args[0] + ".dat");
			userFile.createNewFile();
			FileInputStream fileIn = new FileInputStream(userFile);
			if (fileIn.available() > 0) {
				ObjectInputStream in = new ObjectInputStream(fileIn);
				user = (User) in.readObject();
				in.close();
			}
			if (user == null) {
				System.out.println("THIS SHOULD NEVER HAPPEN");
				user = new User(args[0]);
			}
			// for albumMap (HashMap)
			userFile = new File(userLoadDir + File.separator + args[0] + "Map" + ".dat");
			userFile.createNewFile();
			fileIn = new FileInputStream(userFile);
			if (fileIn.available() > 0) {
				ObjectInputStream in = new ObjectInputStream(fileIn);
				albumMap = (HashMap<String, Boolean>) in.readObject();
				in.close();
			}
			if (albumMap == null) {
//				System.out.println("First time logging in? = create new album name map");
				albumMap = new HashMap<String, Boolean>();
			}
			// for synchronization
			File yourFile = new File(Photos.storeDir + Photos.userFile);
			yourFile.createNewFile(); // if file already exists will do nothing

			fileIn = new FileInputStream(yourFile);
			if (fileIn.available() > 0) {
				ObjectInputStream in = new ObjectInputStream(fileIn);
				tempUser = (ArrayList<User>) in.readObject();
				if (tempUser == null) {
					throw new Exception("is read in as null????");
				}
				in.close();
			}

			for (User u : tempUser) {
				if (u.getName().equals(this.user.getName())) {
					this.syncUser = u;
					break;
				}
			}
			if (this.syncUser == null) {
				//System.out.println("Sync error");
			}

//			printUpdate();

		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return;
		}
	}

	/**
	 * Unused method for debugging
	 */
	private void printUpdate() {
		//System.out.println("Update: " + syncUser);
	}

	/**
	 * Switches to the PhotoController for the target Album
	 * @param a Album to be opened
	 */
	public void openAlbum(Album a) {
		this.write();

		PhotoController al = new PhotoController();
		String fxml = "../view/PhotosPage.fxml";
//		System.out.println(this.user.toString()+"   "+a);
		String[] args = { this.user.getName(), a.getName(), "false" };
		switchScene(this, al, this.s, fxml, args);
	}

	/**
	 * Adds a new album to the user Object
	 * @param e ActionEvent to trigger the method
	 */
	public void addAlbum(ActionEvent e) {
		// add to list
		while (true) {
			TextInputDialog dialog = new TextInputDialog("");
			dialog.setTitle("New Album");
			dialog.setHeaderText("Enter Name of New Album");
			Optional<String> result = dialog.showAndWait(); // get input
			if (result.isPresent()) { // if pressed OK
				if (result.get().equals("")) {
					showAlert(AlertType.ERROR, "Error Dialog", "Name is Empty", "");
					continue;
				}
				if (albumMap.containsKey(result.get())) {
					showAlert(AlertType.ERROR, "Error Dialog", "Album Already Exists", "");
					continue;
				}
				// success
				Album a = new Album(result.get(), user.getName());
				user.addAlbum(a);
				observableAlbumList.add(a);
				albumMap.put(result.get(), true);

				this.syncUser.addAlbum(new Album(null, null));
			}
//			System.out.println("There are " + this.user.getAlbums().size() + " albums");
			return;
		}
	}

	/**
	 * Deletes selected Album from the active User 
	 * @param e ActionEvent to trigger the method
	 */
	public void deleteAlbum(ActionEvent e) {
		// remove from list
		if (albumList.getSelectionModel().getSelectedItem() == null) {
			showAlert(AlertType.ERROR, "Error Dialog", "No Album Selected", "");
			return;
		}
		try {
			// display the confirmation prompt
			Optional<ButtonType> result = showAlert(AlertType.CONFIRMATION, "Confirm Delete", "Delete Album?",
					"Album Picture Links Will Be Deleted Too!");
			if (result.get() == ButtonType.OK) {
				Object o = albumList.getSelectionModel().getSelectedItem();
				// error if selected multiple items
//				System.out.println(o);
				if (!(o instanceof Album))
					throw new Exception("Not an Album Error!!!");
				Album u = (Album) o;
				observableAlbumList.remove(u);
				user.getAlbums().remove(u);
				albumMap.remove(u.getName());
			} else {
				return;
			}
		} catch (Exception ex) {
			System.err.println(ex);
			return;
		}
	}

	/**
	 * Renames the selected Album of the User
	 * @param e ActionEvent to trigger the method
	 */
	public void renameAlbum(ActionEvent e) {
		if (albumList.getSelectionModel().getSelectedItem() == null) {
			showAlert(AlertType.ERROR, "Error Dialog", "No Album Selected", "");
			return;
		}

		Object o = albumList.getSelectionModel().getSelectedItem();
		if (!(o instanceof Album))
			try {
				throw new Exception("Not an Album Error 2 !");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		Album u = (Album) o;

		while (true) {
			// prompt for new name
			TextInputDialog dialog = new TextInputDialog(u.getName());
			dialog.setTitle("Rename Album " + u.getName());
			dialog.setHeaderText("Enter New Name");
			Optional<String> result = dialog.showAndWait(); // get input
			if (result.isPresent()) { // if pressed OK
				if (result.get().equals(u.getName()))
					return;
				if (albumMap.containsKey(result.get())) {
					showAlert(AlertType.ERROR, "Error Dialog", "Album Name Already Exists", "");
					continue;
				}
				albumMap.remove(u.getName());
				u.setName(result.get());
				albumMap.put(result.get(), true);
				albumList.refresh();
			}
			return;
		}
	}

	/**
	 * Searches through all the Photos in the User's Albums for an entered text query in the search TextField 
	 * and displays results using a SearchController
	 * <ul>
	 * <li> Query format is set of {Type}={Value} with conjunctions 'and' and 'or' </li>
	 * </ul>
	 * @param e ActionEvent to trigger the method
	 */
	public void searchAlbums(ActionEvent e) {
		FXMLLoader fxmlLoader = null;
		Scene scene = null;
		// search through albums, return matches
		try {
			if (search.getText().isEmpty()) {
				showAlert(AlertType.ERROR, "Error Dialog", "No Search Query Entered", "");
				return;
			} else {
				Query q = parseCommand(search.getText());
				if (q==null) {
					return;
				}
//				System.out.println("This is what q is :" + q);
				ArrayList<Photo> results = getEligiblePhotos(q);
//				System.out.println("This is what getEP returns :"+results);
				
				fxmlLoader = new FXMLLoader();
				SearchController s = new SearchController(results, this.user.getName(), albumMap);
				fxmlLoader.setController(s);
				fxmlLoader.setLocation(getClass().getResource("../view/SearchPage.fxml"));
				scene = new Scene(fxmlLoader.load(), 600, 400);
				s.start(null, null);
			}
		} catch (Exception ex) {
			System.err.println(ex);
		}
		Stage stage = new Stage();
		stage.setTitle("New Window");
		stage.setScene(scene);
		stage.showAndWait();
		// after the user closes the window
		if (fxmlLoader==null) {
			System.out.println("WHY IS THIS HAPPENING");
			return;
		}
		SearchController s = ((SearchController) fxmlLoader.getController());
//			System.out.println(s);
		Album searchAlbum = s.getAlbum();
//		System.out.println("this is the new album"+searchAlbum);
		if (searchAlbum == null) {
			// either album intentionally not created
			// or there were no results
			//System.out.println("nothing created");
		} else {
//				System.out.println(user);
			user.addAlbum(searchAlbum);
//				System.out.println(observableAlbumList);
			observableAlbumList.add(searchAlbum);
//				System.out.println(albumMap);
			albumMap.put(searchAlbum.getName(), true);
//				System.out.println(this.syncUser);
			this.syncUser.addAlbum(new Album(null, null));
		}
	}

	/**
	 * Helper method for parsing the search command
	 * @param s String command to parse
	 * @return Query object to use to search for results
	 * @throws Exception
	 */
	private Query parseCommand(String s) throws Exception {
		// search by date range
		// search for tags: person=sesh; location=london
		
		Query q = new Query();
		
		String[] temp = s.split(" and | or ");
//		System.out.println(temp);
		for (int i = 0; i < temp.length; i++) {
			int index = temp[i].indexOf("=");
			if (index<0) {
				showAlert(AlertType.ERROR, "Error Dialog", "Query is invalid", "'and' and 'or' must be separated by spaces, tag=value has no spaces");
				return null;
			}
			String a = temp[i].substring(0, index);
			String b = temp[i].substring(index + 1);
			if (a.contains(" ") || b.contains(" ")) {
				showAlert(AlertType.ERROR, "Error Dialog", "Query is invalid", "'and' and 'or' must be separated by spaces, tag=value has no spaces");
				return null;
			}
			if (a.length() == 0 || b.length() == 0) {
				throw new Exception("Illegal Argument");
			}
			q.ans.add(new Tag(a, b));
		}
		
		Pattern pattern = Pattern.compile(" and | or ");
		Matcher m = pattern.matcher(s);
		while (m.find()) {
		    q.andOr.add(m.group());
		}
		if (q.ans.size()!=1+q.andOr.size()) {
			showAlert(AlertType.ERROR, "Error Dialog", "Query is invalid", "'and' and 'or' must be separated by spaces, tag=value has no spaces");
			return null;
		}
		return q;
	}
	
	/**
	 * Collects the photos that have any tag which matches the search query
	 * <ul>
	 * <li> No parentheses allowed, all boolean logic statements are evaluated in order </li>
	 * </ul>
	 * @param q Query object to match
	 * @return ArrayList<Photo> of eligible results
	 */
	private ArrayList<Photo> getEligiblePhotos(Query q) {
		ArrayList<Photo> photos = new ArrayList<Photo>();
		
		photos = or(q.ans.get(0), photos);
		int i = 1;
		for (String s : q.andOr) {
			if (s.equals("and")) {
				photos = and(q.ans.get(i), photos);
			} else if (s.equals("or")) {
				photos = or(q.ans.get(i), photos);
			} else {
//				System.out.println("Exception here");
			}
			i++;
		}
		return photos;
	}
	
	/**
	 * Applies logical 'AND' on a Tag paired with an ArrayList of Photos
	 * @param tq Tag object to find
	 * @param curr ArrayList<Photo> to search
	 * @return ArrayList<Photo> of results
	 */
	private ArrayList<Photo> and(Tag tq, ArrayList<Photo> curr) {
		ArrayList<Photo> photos = new ArrayList<Photo>();
		for (Album a : this.user.getAlbums()) {
			for (Photo p : a.getPhotos()) {
				if (!curr.contains(p)) {
					continue;
				}
				outerloop:
				for (Tag t : p.getTags()) {
					if (tq.equals(t)) {
						photos.add(p);
						break outerloop;
					}
				}
			}
		}
		return photos;
	}
	
	/**
	 * Applies logical 'OR' on a Tag paired with an ArrayList of Photos
	 * @param tq Tag object to find
	 * @param curr ArrayList<Photo> to search
	 * @return ArrayList<Photo> of results
	 */
	private ArrayList<Photo> or(Tag tq, ArrayList<Photo> curr) {
		ArrayList<Photo> photos = new ArrayList<Photo>();
		for (Album a : this.user.getAlbums()) {
			for (Photo p : a.getPhotos()) {
				outerloop:
				for (Tag t : p.getTags()) {
					if (tq.equals(t)) {
						photos.add(p);
						break outerloop;
					}
				}
			}
		}
		photos.addAll(curr);
		Set<Photo> hs = new HashSet<>();
		hs.addAll(photos);
		photos.clear();
		photos.addAll(hs);
		return photos;
	}
	
	/**
	 * Confirms logout of active User and writes his/her data to file
	 * Switches to the LoginController
	 * @param e ActionEvent to trigger this method
	 */
	public void logout(ActionEvent e) {
		Optional<ButtonType> result = showAlert(AlertType.CONFIRMATION, "Logout", "Logout?", "Changes will be saved");
		if (result.get() == ButtonType.OK) {
			// go back to login screen
			write();
			LoginController a = new LoginController();
			String fxml = "../view/LoginPage.fxml";
			switchScene(this, a, this.s, fxml); // this will call write()
		}
	}
	
	/**
	 * Serializes the data to the three files associated with the active User
	 */
	public void write() {
		try {
			// write (individual) user to file
			FileOutputStream fos = new FileOutputStream(userLoadDir + File.separator + user.getName() + ".dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this.user);
			oos.close();

			// write userMap to a file
			fos = new FileOutputStream(userLoadDir + File.separator + user.getName() + "Map" + ".dat");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(albumMap);
			oos.close();

			// write (individual) user to file
			fos = new FileOutputStream(Photos.storeDir + Photos.userFile);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(tempUser);
			oos.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Displays a confirmation and writes session data to file
	 * @return boolean determining if exit was successful or not
	 */
	@Override
	public boolean onExit() {

		this.write();
		Optional<ButtonType> result = AbstractController.showConfirmation("Exit?", "Changes have been saved.");
		if (result.get() == ButtonType.OK) {
			return true;
		} else {
			return false;
		}
	}
}
