/**
 * Author: Kelvin Liu, Ezra Ablaza
 */
package controller;
import model.Album;
import model.Photo;
import model.User;


import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;

import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * This controller is used when the user wants to search for photos in their albums
 * A new window pops up from the album window
 * @author kelvi
 */
public class SearchController extends AbstractController
{
	@FXML
	Button backButton;
	@FXML
	Button createNewAlbum;
	@FXML
	ScrollPane resultsView;
	ObservableList<Photo> observablePhotosList = FXCollections.<Photo>observableArrayList();
	
	

	
	public ArrayList<Photo> photos;
	public Album a;
	public String userName;
	public HashMap<String, Boolean> albumMap;
	
	/**
	 *  Inner class to handle the display and focus of Photos in the resultsView ScrollPane
	 * @author Ezra Ablaza
	 *
	 */
	private class LimitDisplay extends GridPane{
		
		private Photo p;
		private ImageView view;
		private Text cap;
		private Text name;
		
		/**
		 * Returns a LimitDisplay instance object with proper formatting containing
		 * <ul>
		 * <li> An ImageView of the Photo </li>
		 * <li> The caption of the Photo in Text format, if it exists </li>
		 * <li> The filename of the Photo in Text format </li>
		 * </ul>
		 * @param p Photo object to display
		 * @param imgWidth int parameter to control size
		 * @throws Exception
		 */	
		public LimitDisplay(Photo p, int imgWidth) throws Exception {
			
			try {
					//System.out.println(p.toString());
					this.p = p;
					this.view = new ImageView(p.getImage());
					this.view.setFitWidth(imgWidth);
					this.view.setFitHeight(150);
	                this.view.setPreserveRatio(true);
	                this.view.setSmooth(true);
					
					String caption = p.getCaption();
					if(caption != null && !caption.isEmpty()) {
						int charNum = caption.length();
						if(charNum <= 20) {
							this.cap = new Text(caption);
						} else {
							this.cap = new Text(caption.substring(0, 20) + "...");
						}
	
					} else {
						this.cap = new Text("");
					}
					
					String n = p.getShortPath();
					if(n.length() <= 20) {
						this.name = new Text(n);
					} else {
						String ending = n.substring(n.indexOf("."), n.length());
						this.name = new Text(n.substring(0, 16) + "..." + ending);
					}
				
					this.cap.setWrappingWidth(imgWidth);
					this.cap.setTextAlignment(TextAlignment.CENTER);
					
	
					this.add(view, 0, 0);
					this.add(cap, 0, 1);
					this.add(name,  0, 2);
					
					this.setPrefHeight(this.view.getFitHeight());
					GridPane.setHalignment(this.view, HPos.CENTER);
					GridPane.setHalignment(this.cap, HPos.CENTER);
					GridPane.setHalignment(this.name, HPos.CENTER);
					this.setAlignment(Pos.CENTER);
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	
	public SearchController(ArrayList<Photo> p, String userName, HashMap<String, Boolean> albumMap) {
		this.setPhotosAndMetaData(p, userName, albumMap);
	}
	
	/**
	 * This method is used to pass important data from the AlbumController to this instance of the SearchController
	 * @param p
	 * @param userName
	 * @param albumMap
	 */
	public void setPhotosAndMetaData(ArrayList<Photo> p, String userName, HashMap<String, Boolean> albumMap) {
		this.photos = p;
		this.userName = userName;
		this.albumMap = albumMap;
	}
	
	@Override
	public void start(Stage stage, String... args) throws Exception
	{
		if (photos == null) {
			throw new Exception("Photos was never set!!!");
		} else if (userName==null || albumMap==null) {
			throw new Exception("Other arguments not set!!!");
		}
		//System.out.println("This is what 2nd controller received  " + this.photos);
		observablePhotosList = FXCollections.<Photo>observableArrayList(this.photos);
		
		
		//System.out.println(this.photos.size());
		this.renderPhotos();
	}
	
	/**
	 * Loads LimitDisplay objects of Photos into the resultsView ScrollPane
	 */
	public void renderPhotos() {

		FlowPane photoDisplay = new FlowPane();
		photoDisplay.setPadding(new Insets(15, 15, 15, 15));
        photoDisplay.setHgap(5);
        photoDisplay.setPrefWrapLength(400);
        
        int width = 115;
        
        if(this.photos.size() >= 9) {
        	width = 100;
        }
        if(this.photos.size() > 12) {
        	width = 80;
        }
        if(this.photos.size() > 16) {
        	width = 50;
        	
        }
        if(this.photos.size() > 32) {
        	width = 35;
        }
        
        boolean allLoad = true;
        
        Iterator<Photo> iter = this.photos.iterator();

		
		while(iter.hasNext()) {
			try {
				Photo p = iter.next();
				LimitDisplay display =  new LimitDisplay(p, width);
				photoDisplay.getChildren().addAll(display);
			} catch (Exception e) {
				AbstractController.showError(e);
				allLoad = false;
				iter.remove();
				continue;
			}
			
			//System.out.println("loaded");
		}
		
		if(allLoad != true) {
			AbstractController.showWarning("Missing content", "There was an error adding "
					+ "one or more of the photos in this album.  They have been removed.");
		}
		resultsView.setContent(photoDisplay);

	}
	
	
	/**
	 * Return to the albumController instance
	 * Closes this instance of stage
	 * Album should already be created before this method is called
	 * @param e
	 */
	public void back(ActionEvent e) {
		Stage stage = (Stage) backButton.getScene().getWindow();
		stage.close();
	}
	
	/**
	 * Called when the user wants to create an album from the results of the search query
	 * Happens on createNewAlbum button click
	 * 
	 * @param e
	 * 
	 */
	public void createNewAlbum(ActionEvent e) {
		if (this.photos==null || this.photos.isEmpty()) {
			showAlert(AlertType.ERROR, "Error Dialog", "There Are Zero Results!","");
			return;
		}
//		Album a = null;
		while (true) {
			// show alert to get user input on album name
			TextInputDialog dialog = new TextInputDialog("");
			dialog.setTitle("Create New Album");
			dialog.setHeaderText("Enter Name of New Album");
			Optional<String> result = dialog.showAndWait(); // get input
			if (result.isPresent()) { // if pressed OK
				if (result.get().equals("")) {
					showAlert(AlertType.ERROR, "Error Dialog", "Name is Empty","");
					continue;
				}
				if (this.albumMap.containsKey(result.get())) {
					showAlert(AlertType.ERROR, "Error Dialog", "Album Already Exists","");
					continue;
				}
				// success
				a = new Album(result.get(), this.userName, photos);
				Stage stage = (Stage) backButton.getScene().getWindow();
				stage.close();
				break;
			}
			break;
		}
	}
	
	public Album getAlbum() {
		return a;
	}
	
	@Override
	public boolean onExit() 
	{
		return true;
	}
	
}
