package controller;
import model.User;


import javafx.fxml.FXML;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Optional;

import app.Photos;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 * Controller class which handles the login of Users
 * @author Ezra Ablaza
 *
 */
public class LoginController extends AbstractController
{
	/**
	 * String with the scene title
	 */
	public static String sceneTitle = "Login";
	
	@FXML
	private TextField usernameText;
	
	
//	private Stage s;
	
	
	
	/* (non-Javadoc) Starts the LoginController  
	 * @see controller.AbstractController#start(javafx.stage.Stage, java.lang.String[])
	 * @param stage Stage to maintain
	 * @param args
	 */
	@Override
	public void start(Stage stage, String... args)
	{
		this.s = stage;
		try
		{	
			this.loadUserMap();
		}
		catch(IOException ioe)
		{
			showError(ioe);
		}
		catch(ClassNotFoundException cnfe)
		{
			showError(cnfe);
		}
		
	}
	
	
	/**
	 * Loads userMap from file
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private void loadUserMap() throws IOException, ClassNotFoundException
	{
		File uf = new File(Photos.userMapDirString);
		uf.createNewFile();
		FileInputStream fileIn = new FileInputStream(Photos.userMapDirString);
		
		if (fileIn.available() > 0) {
			ObjectInputStream in = new ObjectInputStream(fileIn);
			Photos.userMap = (HashMap<String, Boolean>) in.readObject();
			in.close();
		}
		if (Photos.userMap == null)
			Photos.userMap = new HashMap<String, Boolean>();
	}
	
	/**
	 * Attempts to login with the username entered in the usernameText TextField
	 * <ul>
	 * <li> Will switch to AdminController if username is "admin" </li>
	 * <li> Will switch to AlbumController if username is otherwised registered </li>
	 * <li> Will show a Warning if unsuccessful </li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void attemptLogin(ActionEvent event)
	{
		String userName = usernameText.getText();
		
		
		if(userName.equals("admin"))
		{
			AdminController ac = new AdminController();
			String fxml = "../view/AdminPage.fxml";
			switchScene(this, ac, this.s, fxml);
		}	
		else
		{
			boolean isUser = this.findUser(userName);
			
			if(isUser == true)
			{
				AlbumController al = new AlbumController();
				String fxml = "../view/AlbumPage.fxml";
				switchScene(this, al, this.s, fxml, userName);
			}
			else
			{
				
				AbstractController.showWarning("User not found", "The specified user was not found.  Please re-enter.");
				clearFields();
			}
		}
		return;

	}
	
	/**
	 * Searches userMap to find a User by entered name
	 * @param name String representing username
	 * @return boolean representing if search was successful
	 */
	private boolean findUser(String name)
	{
		return Photos.userMap.containsKey(name);
	}

	/**
	 * Clears the TextFields of the window
	 */
	public void clearFields()
	{
		usernameText.clear();
	}
	
	/**
	 * Always allows an exit from the window
	 */
	@Override
	public boolean onExit() 
	{
		
		return true;
	}
	
}
