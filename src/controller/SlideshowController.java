package controller;

import java.util.ArrayList;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import model.Photo;
/**
 * Class to handle Slideshow of Photos in an Album
 * @author Ezra Ablaza
 *
 */
public class SlideshowController {
	@FXML ImageView slideView;
	@FXML Button rightButton;
	@FXML Button leftButton;
	
	private Photo[] slides;
	private int currentindex;
	private int maxindex;
	
	/**
	 * Returns an instance of SlideshowController initialized with the passed ArrayList&lt;Photo&gt; and the targeted Photo
	 * @param slides ArrayList&lt;Photo&gt; that contains all the Photos for the slideshow
	 * @param currentSlide Photo which will act as the starting slide
	 */
	public SlideshowController(ArrayList<Photo> slides, Photo currentSlide) {
		this.slides = (Photo []) slides.toArray(new Photo[slides.size()]);
		this.currentindex = slides.indexOf(currentSlide);
		this.maxindex = this.slides.length -1;
		
	}
	
	/**
	 * Starts the slideshow at the initial slide
	 */
	public void start() {
		this.render(this.currentindex);
	}
	
	/**
	 * Renders the referenced Photo as a viewable slide in the slideView ImageView
	 * @param index int denoting which Photo to render
	 */
	private void render(int index) {
		try {
			Photo p = this.slides[index];
			slideView.setImage(p.getImage());
		} catch (Exception e) {
			AbstractController.showError(e);
		}
	}
	
	/**
	 * Renders the Photo corresponding to the index immediately below the current index
	 * <ul>
	 * <li> Enables wrap around navigation</li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void getPrevSlide(Event event) {
		this.currentindex--;
		if(this.currentindex < 0) {
			this.currentindex = this.maxindex;
		}
		this.render(this.currentindex);
	}
	
	/**
	 * Renders the Photo corresponding to the index immediately above the current index
	 * <ul>
	 * <li> Enables wrap around navigation</li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void getNextSlide(Event event) {
		this.currentindex++;
		if(this.currentindex > this.maxindex) {
			this.currentindex = 0;
		}
		this.render(this.currentindex);
	}
	
	/**
	 * Exits the stage after the User Confirms
	 * @return boolean demonstrating if the User confirmed the exit or not
	 */
	public Photo onExit() {
		Optional<ButtonType> result = AbstractController.showConfirmation("Exit?", "Exit the slideshow?");
		if (result.get() == ButtonType.OK) {
			return this.slides[this.currentindex];
		} else {
			return null;
		}
	}
	
	
}
