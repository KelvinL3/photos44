
package controller;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Collectors;


import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

/**
 * Controller class which handles Photo management within a User's Album
 * @author Ezra Ablaza
 *
 */
public class PhotoController extends AbstractController 
{
	//? Grid of thumbnails and names
	//displays current image
	@FXML ImageView imageView;
	//display fields
	@FXML ScrollPane scrollWindow;
	@FXML Label displayTitle;
	
	@FXML TextFlow photoFileName;
	@FXML TextFlow photoCaption;
	@FXML TextFlow photoDateTime;
	@FXML ScrollPane photoTagList;
	
	@FXML Button addPhotoButton;
	@FXML Button deletePhotoButton;
	@FXML Button captionPhotoButton;
	@FXML Button addTagButton;
	@FXML Button deleteTagButton;
	@FXML Button movecopyPhotoButton;
	@FXML Button slideshowButton;
	@FXML Button backButton;
	
	private String userName;
	private String albumName;
	private User user;
	private Album album;
	private Photo focusedPhoto;
	
	


	private ObservableList<Photo> photoList;
	
	/**
	 * Inner class to handle the display and focus of Photos in the scrollWindow ScrollPane
	 * @author Ezra Ablaza
	 *
	 */
	private class Display extends GridPane{
		
		private Photo p;
		private ImageView view;
		private Text cap;
		private Text name;
		
		
		/**
		 * Returns a Display instance object with proper formatting and attached listeners which contains
		 * <ul>
		 * <li> An ImageView of the Photo </li>
		 * <li> The caption of the Photo in Text format, if it exists </li>
		 * <li> The filename of the Photo in Text format </li>
		 * </ul>
		 * @param p Photo object to display
		 * @param imgWidth int parameter to control size
		 * @throws Exception
		 */
		public Display(Photo p, int imgWidth) throws Exception {
		
				try {
						//System.out.println(p.toString());
						this.p = p;
						this.view = new ImageView(p.getImage());
						this.view.setFitWidth(imgWidth);
						this.view.setFitHeight(150);
                        this.view.setPreserveRatio(true);
                        this.view.setSmooth(true);
						
						String caption = p.getCaption();
						if(caption != null && !caption.isEmpty()) {
							int charNum = caption.length();
							if(charNum <= 20) {
								this.cap = new Text(caption);
							} else {
								this.cap = new Text(caption.substring(0, 20) + "...");
							}

						} else {
							this.cap = new Text("");
						}
						
						String n = p.getShortPath();
						if(n.length() <= 20) {
							this.name = new Text(n);
						} else {
							String ending = n.substring(n.indexOf("."), n.length());
							this.name = new Text(n.substring(0, 16) + "..." + ending);
						}
					
						this.cap.setWrappingWidth(imgWidth);
						this.cap.setTextAlignment(TextAlignment.CENTER);
						

						this.add(view, 0, 0);
						this.add(cap, 0, 1);
						this.add(name,  0, 2);
						
						this.setPrefHeight(this.view.getFitHeight());
						GridPane.setHalignment(this.view, HPos.CENTER);
						GridPane.setHalignment(this.cap, HPos.CENTER);
						GridPane.setHalignment(this.name, HPos.CENTER);
						this.setAlignment(Pos.CENTER);
						this.setStyle("-fx-border-width: 5;" + 
				    			"-fx-border-color: rgba(30, 144, 255, 0.0);" + 
				    			"-fx-border-radius: 5;");
						this.view.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
							if (isNowFocused) {
								
								focusedPhoto = this.p;	
								fillPhotoFields(this.p);
								//System.out.println(this.getChildren().get(0).getClass());
								this.setStyle("-fx-border-width: 5;" + 
										"-fx-border-color: rgba(30, 144, 255, 0.8);"+ 
					    			"-fx-border-radius: 5;");
							} else {
					    	this.setStyle("-fx-border-width: 5;" + 
					    			"-fx-border-color: rgba(30, 144, 255, 0.0);" + 
					    			"-fx-border-radius: 5;");
							}
						});
	
						
						this.view.setOnMouseClicked(new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent mouseEvent) {
								view.requestFocus();
								if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
									if (mouseEvent.getClickCount() == 2) {
									}
								}
							}
						});	 
					
				} catch (Exception e) {
					throw e;
				}
		}
		
		/**
		 * Returns a reference to the associated Photo
		 * @return Photo referenced by this instance
		 */
		public Photo getPhoto() {
			return this.p;
		}
		
	}
	
	/**
	 * Inner class to handle the display of Tags in the photoTagList scrollPane
	 * @author Ezra Ablaza
	 *
	 */
	private class TagBox extends HBox {
		private Tag t;
		
		/**
		 * Returns a TagBox instance with proper formatting which contains the associated Tag information
		 * @param t Tag object to display
		 */
		public TagBox(Tag t) {
			this.t = t;
			Text tagContent = new Text(t.toString());
			
			tagContent.setFont(Font.font("Verdana", FontWeight.THIN, 12));
			tagContent.setFill(Color.WHITE);
			tagContent.setTextAlignment(TextAlignment.CENTER);

			this.setMinHeight(20);
			this.setMaxHeight(20);
			this.setMinWidth(tagContent.getLayoutBounds().getWidth() + 10);
			this.setAlignment(Pos.CENTER);

			this.setStyle("-fx-background-color: rgba(30, 144, 255, 0.6);");
			
			
			
			

			this.getChildren().add(tagContent);
		}
		
	}
	
	/**
	 * Comparator implementation to allow alphabetical ordering of Photos
	 * @author Ezra Ablaza
	 *
	 */
	class AlphabeticComparator implements Comparator<Photo>
	{
		  /**
		   * Lexicographically compares two Photo objects' filenames
		   * @param p1 Photo object to compare with p2
		   * @param p2 Photo object to compare with p1
		   * @return int showing lexicographical distance between p1 and p2
		   */
		  public int compare(Photo p1, Photo p2) 
		  {
		    //lexical order
			String s1 = p1.getShortPath();
			String s2 = p2.getShortPath();
			
		    int val =  s1.toLowerCase().compareTo(s2.toLowerCase());
		    if(val == 0) 
		    {
		    	//check for uppercase characters
		    	int s1upper = 0;
		    	int s2upper = 0;
		    	//iteration to form unique numbers
		    	for(int i = s1.length() - 1; i >= 0; i--)
		    	{
		    		char c = s1.charAt(i);
		    		if(c == Character.toUpperCase(c))
		    		{
		    			s1upper += Math.pow(2, s1.length() - i);
		    		}
		    		
		    	}
		    	for(int i = s2.length() -1; i >= 0; i--)
		    	{
		    		char c = s2.charAt(i);
		    		if(c == Character.toUpperCase(c))
		    		{
		    			s2upper += Math.pow(2, s2.length() - i);
		    		}
		    		
		    	}
		    	val = s1upper - s2upper;
		    } 
		    
		    return val;
		  }
	}
		
	/* (non-Javadoc) Starts the PhotoController and sets up the ObservableList of Photos in the Album  
	 * @see controller.AbstractController#start(javafx.stage.Stage, java.lang.String[])
	 * @param stage Stage to maintain
	 * @param args contains the following String arguments
	 * <ol>
	 * <li> the username of the active User </li>
	 * <li> the name of the opened Album </li>
	 * </ol>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage stage, String... args) {
		
		this.s = stage;
		this.userName = args[0];
		this.albumName = args[1];
		//this.isSearch = Boolean.getBoolean(args[2]);
		
		try {
			//retrieve user file
			this.user = this.loadUserFile(this.userName);
			//System.out.println("debug "+this.userName+this.user+"  "+this.albumName);

			this.album = this.user.getAlbum(this.albumName);
			this.photoList = FXCollections.observableArrayList(this.album.getPhotos());
			
			Comparator<Photo> comparator = new AlphabeticComparator();
			
			this.photoList.addListener((ListChangeListener.Change<? extends Photo> p) -> {
				while((p.next()))
				{
					if(p.wasAdded() || p.wasRemoved())
					{
						FXCollections.sort(this.photoList, comparator);

						//System.out.println(this.photoList.size());
					}
					
				}
				this.loadPhotos();


			});
			//first time load		
			if(photoList.size() > 0) {
				this.selectPhoto(this.photoList.get(0));
			}
			displayTitle.setText("Photos from " + this.albumName);
		}
		catch(Exception e) {
			//print trace
			showError(e);
		}
	}
	
	/**
	 * Loads Display objects of Photos into the scrollWindow ScrollPane
	 */
	private void loadPhotos()	{
		
		
		FlowPane photoDisplay = new FlowPane();
		photoDisplay.setPadding(new Insets(15, 15, 15, 15));
        photoDisplay.setHgap(5);
        photoDisplay.setPrefWrapLength(400);
        
        int width = 115;
        
        if(photoList.size() >= 9) {
        	width = 100;
        }
        if(photoList.size() > 12) {
        	width = 80;
        }
        if(photoList.size() > 16) {
        	width = 50;
        	
        }
        if(photoList.size() > 32) {
        	width = 35;
        }
        
        boolean allLoad = true;
        
        Iterator<Photo> iter = this.photoList.iterator();

		
		while(iter.hasNext()) {
			try {
				Photo p = iter.next();
				Display display =  new Display(p, width);
				photoDisplay.getChildren().addAll(display);
			} catch (Exception e) {
				AbstractController.showError(e);
				allLoad = false;
				iter.remove();
				continue;
			}
			
			//System.out.println("loaded");
		}
		
		if(allLoad != true) {
			AbstractController.showWarning("Missing content", "There was an error adding "
					+ "one or more of the photos in this album.  They have been removed.");
		}
		scrollWindow.setContent(photoDisplay);

	}
	
	/**
	 * Loads all the tags of the requested Photo as TagBox objects in the photoTagList ScrollPane
	 * @param p Photo to use
	 */
	private void loadTags(Photo p)	{
		
		
		FlowPane tagDisplay = new FlowPane();
		tagDisplay.setPadding(new Insets(5, 5, 5, 5));
        tagDisplay.setHgap(5);
        tagDisplay.setVgap(5);
        tagDisplay.setPrefWidth(photoTagList.getWidth());
        
        ArrayList<Tag> tags = p.getTags();
        
        Iterator<Tag> iter = tags.iterator();

		
		while(iter.hasNext()) {
			Tag t = iter.next();
			TagBox tb =  new TagBox(t);
			tagDisplay.getChildren().addAll(tb);
	
		}
		
		photoTagList.setContent(tagDisplay);
	}
	
	/**
	 * Shifts window focus to the specified Photo
	 * @param p Photo target
	 */
	public void selectPhoto(Photo p)
	{
		loadPhotos();
		FlowPane f = (FlowPane) scrollWindow.getContent();
		Object[] c = (Object []) f.getChildrenUnmodifiable().toArray();
		for(Object o: c) {
			if(o instanceof Display) {
				Display d = (Display) o;
				if(d.getPhoto().equals(p)) {
					d.view.requestFocus();
					return;
				} 
			}
		}
		//didn't find anything
		this.clearFields();
		this.focusedPhoto = null;
		

		
	}
	
	/**
	 * Adds a new Photo to the Album if it doesn't already exist, specified by a FileChooserDialog
	 * @param event ActionEvent to trigger the method
	 */
	public void addPhoto(ActionEvent event)
	{
	     FileChooser fileChooser = new FileChooser();
         
         //Set extension filter

         FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
         FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
         fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
         //Show open file dialog
         File file = fileChooser.showOpenDialog(null);
         if(file == null) {
        	 return;
         }
         try 
         {
        	 Path currentDir = Paths.get("").toAbsolutePath();
             Path imagePath = Paths.get(file.getAbsolutePath());
             Path rImagePath = currentDir.relativize(imagePath);

             Photo p = new Photo(rImagePath, this.albumName);
             if(!this.photoList.contains(p)) {
            	 this.photoList.add(p);
             } else {
            	 AbstractController.showWarning("Duplicate Photo", "Target photo already exists in this album.");
             }
             this.selectPhoto(p);
             
         } catch (Exception ex) 
         {
//        	 System.err.println(ex);
        	 AbstractController.showError(ex);
         }
	}
	
	/**
	 * Deletes the currently focused Photo from the Album 
	 * <ul>
	 * <li> Allows active User to cancel deletion </li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void deletePhoto(ActionEvent event) {
		if(this.photoList.size() > 0)
		{
			Optional<ButtonType> result = AbstractController.showConfirmation("Delete?", "Do you want to "
					+ "remove the selected photo from this album?");
			
			if(result.get() == ButtonType.OK)
			{
				
	
				Iterator<Photo> iter = this.photoList.iterator();
				while (iter.hasNext()) {
				   Photo p = iter.next();
				   if(p.equals(this.focusedPhoto)) {
					   
					   iter.remove();
				   }
				}
				this.selectPhoto(null);

			} else {
				AbstractController.showWarning("Deletion Cancelled", "The action was discontinued.");
			}
		} else {
			AbstractController.showWarning("No Remaining Photos", "There are no more photos to delete.");
		}
		
	}
	
	/**
	 * Makes a caption for the focused Photo through a TextInputDialog
	 * @param event ActionEvent to trigger the method
	 */
	public void makeCaption(ActionEvent event) {
		if(this.focusedPhoto != null) {
			TextInputDialog dialog = new TextInputDialog("Make Caption");
			dialog.setTitle("Make a caption");
			dialog.setHeaderText("Add a caption for " + this.focusedPhoto.getPath());
			dialog.setContentText("Write a caption of at least 4 characters and no more than 120 characters:");
	
			final Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
			final TextField input = (TextField) dialog.getEditor();
			okButton.setDisable(true);
			input.textProperty().addListener((observable, oldValue, newValue) -> {
			    okButton.setDisable(newValue.trim().length() < 4 || newValue.trim().length() > 120); 
			});
			//get the response value
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){
				this.focusedPhoto.changeCaption((String) result.get());
			
			}				
		
		} else {
			AbstractController.showWarning("No Photo Selected", "There is no target photo to add a caption to.");
		}
		//reload
		this.selectPhoto(this.focusedPhoto);
		
	}
	
	/**
	 * Adds a Tag to the focused Photo, created through a TextInputDialog
	 * <ul>
	 * <li>Tags must be specified as "{Type}={Value}"</li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void addTag(ActionEvent event) {
		if(this.focusedPhoto != null) {
			TextInputDialog dialog = new TextInputDialog("Tag input");
			dialog.setTitle("Add Tag");
			dialog.setHeaderText("Add a tag for " + this.focusedPhoto.getPath());
			dialog.setContentText("Please enter the tag in the format of {type}={value}, \n "
					+ "e.g. location=France:");
			
			
	
			final Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
			final TextField input = (TextField) dialog.getEditor();
			okButton.setDisable(true);
			input.textProperty().addListener((observable, oldValue, newValue) -> {
				
				int eqs = (int) newValue.chars().filter(num -> num == '=').count();
	
			    okButton.setDisable(newValue.trim().length() < 5 || newValue.trim().length() > 120 ||
				eqs != 1 || (newValue.indexOf("=") == 0 || newValue.indexOf("=") == newValue.length() - 1));
	
			});
			
			//get the response value
			Optional<String> result = dialog.showAndWait();
			if(result.isPresent()) {
				String entry = result.get();
				String[] components = entry.toLowerCase().split("="); //should have 2.
				this.focusedPhoto.addTag(new Tag(components[0], components[1]));
			}
		} else {
			AbstractController.showWarning("No Photo Selected", "There is no target photo to add a tag to.");
		}
			
		//reload
		this.selectPhoto(this.focusedPhoto);
		//StringProperty is trash, just hard call update here
		
	}
	
	/**
	 * Deletes a Tag from the focused Photo via a ChoiceDialog
	 * @param event ActionEvent to trigger the method
	 */
	public void deleteTag(ActionEvent event) {
		ArrayList<Tag> tags = this.focusedPhoto.getTags();
		if(tags.size() > 0) 
		{
			ArrayList<String> strs = (ArrayList<String>) this.focusedPhoto.getTags().stream().map(t -> 
				t.toString()).collect(Collectors.toList());
	
			ChoiceDialog<String> dialog = new ChoiceDialog<String>(strs.get(0), strs);
			dialog.setTitle("Delete Tag");
			dialog.setHeaderText("Delete tag from selected photo");
			dialog.setContentText("Choose a tag to delete:");
	
			// Traditional way to get the response value.
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){
				
				String target = result.get();
				String[] components = target.split("="); //should have 2.
				this.focusedPhoto.removeTag(new Tag(components[0], components[1]));
				
				AbstractController.showConfirmation("Deleted Tag", "Tag: " + result.get() + " has been deleted.");
			}
			
		} else {
			AbstractController.showWarning("No Remaining Tags", "There are no more tags to delete on this photo.");
		}

		//reload
		this.selectPhoto(this.focusedPhoto);
		
	}
	
	/**
	 * Moves or shallow copies the focused Photo to a different Album, depending on option chosen in AlertDialog
	 * <ul>
	 * <li>Allows the active User to cancel action</li>
	 * </ul>
	 * @param event ActionEvent to trigger the method
	 */
	public void movecopyPhoto(ActionEvent event) {
		ArrayList<Album> albums = this.user.getAlbums();
		if(this.focusedPhoto != null && albums.size() > 1) {
			
			
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Move/Copy Selection");
			alert.setHeaderText("Move/Copy " + this.focusedPhoto.getPath());
			alert.setContentText("Choose whether to move or copy the selected photo.");

			ButtonType buttonTypeMove = new ButtonType("Move");
			ButtonType buttonTypeCopy = new ButtonType("Copy");
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(buttonTypeMove, buttonTypeCopy, buttonTypeCancel);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeMove){
				this.albumSelect(false);
				return;
			} else if (result.get() == buttonTypeCopy) {
			    this.albumSelect(true);
			    return;
			} else {
				return;
			}
			
		} else if(this.focusedPhoto == null) {
			AbstractController.showWarning("No Selected Photo", "No photo has been selected to move or copy.");		
		} else if(albums.size() <= 1) {
			AbstractController.showWarning("No Candidate Albums", "No other albums are availble to move or copy seected photo to.");		
		}
		
	}
	
	/**
	 * Helper method to select an Album to move or copy to via a ChoiceDialog
	 * <ul>
	 * <li>Allows active User to cancel the action </li>
	 * </ul>
	 * @param keepOriginal boolean to determine whether the Photo should be moved or copied
	 */
	private void albumSelect(boolean keepOriginal) {

		ArrayList<String> strs = (ArrayList<String>) this.user.getAlbums().stream().map(a -> 
			a.getName()).filter(s -> !s.equals(this.album.getName())).collect(Collectors.toList());
		
		ChoiceDialog<String> dialog = new ChoiceDialog<String>(strs.get(0), strs);
		
		if(keepOriginal == true) {
			dialog.setTitle("Copy Photo");
			dialog.setHeaderText("Copy selected photo from this album to another owned album");
			dialog.setContentText("Choose an album to copy to:");

		} else {
			dialog.setTitle("Move Photo");
			dialog.setHeaderText("Transfer selected photo from this album to another owned album");
			dialog.setContentText("Choose an album to move to:");

		}
		
		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			if(keepOriginal == true) {
				try {
					Optional<ButtonType> confirm = AbstractController.showConfirmation("Copy?", "Do you want to "
							+ "copy the photo to this album: " + result.get() + "?");
					
					if(confirm.get() == ButtonType.OK)
					{
						Album dest = this.user.getAlbum(result.get());
						dest.addPhoto(this.focusedPhoto);
						this.selectPhoto(this.focusedPhoto);
						AbstractController.showConfirmation("Photo Copied", "Photo has been copied to destination: " + 
								dest.getName());
						return;
					} else {
						this.selectPhoto(this.focusedPhoto);
						AbstractController.showConfirmation("Move/Copy Cancelled", "The action has been discontinued.");
						return;
					}
				} catch (Exception e) { 
					AbstractController.showError(e);
				}
				
			} else {
				try {
					Optional<ButtonType> confirm = AbstractController.showConfirmation("Move?", "Do you want to "
							+ "move the photo to this album: " + result.get() + "?");
					
					if(confirm.get() == ButtonType.OK)
					{
						Album dest = this.user.getAlbum(result.get());
						dest.addPhoto(this.focusedPhoto);
						
						Iterator<Photo> iter = this.photoList.iterator();
						while (iter.hasNext()) {
						   Photo p = iter.next();
						   if(p.equals(this.focusedPhoto)) {
							   
							   iter.remove();
						   }
						}
						this.selectPhoto(null);

						AbstractController.showConfirmation("Photo Moved", "Photo has been moved to destination: " + 
								dest.getName());
						return;
					} else {
						this.selectPhoto(this.focusedPhoto);

						AbstractController.showConfirmation("Move/Copy Cancelled", "The action has been discontinued.");
						return;
					}
				} catch (Exception e) {
					AbstractController.showError(e);
				}
				
			}
			
		} else {
			AbstractController.showWarning("Move/Copy Cancelled", "The action has been discontinued.");
		}
	}
	
	/**
	 * Creates a SlideshowController instance for the current Album's Photos and starts a slideshow at 
	 * the focused Photo
	 * @param event ActionEvent to trigger the method
	 */
	public void startSlideshow(ActionEvent event) {
		
		ArrayList<Photo> slides = (ArrayList<Photo>)this.photoList.stream().collect(Collectors.toList());
		if(!slides.isEmpty()) {
			Optional<ButtonType> result = AbstractController.showConfirmation("Start?", "Do you want to "
					+ "start the slideshow?");
			
			if(result.get() == ButtonType.OK)
			{
				
				Photo startingPhoto;
				if(this.focusedPhoto == null) {
					this.selectPhoto(this.photoList.get(0));
				}
				 startingPhoto = this.focusedPhoto;
				
				SlideshowController sc = new SlideshowController(slides, startingPhoto);
			        try {
			            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Slideshow.fxml"));
			            loader.setController(sc);
			            Parent p  = (Parent) loader.load();	      
			            
			            Stage stage = new Stage();
			            sc.start();
			            
			            Scene scene = new Scene(p);
			            scene.setOnKeyPressed(e -> {
			                if (e.getCode() == KeyCode.RIGHT) {
			                	sc.getNextSlide(e);
			                } else if (e.getCode() == KeyCode.LEFT) {
			                	sc.getPrevSlide(e);
			                }
			            });
			            stage.setResizable(false);
			            stage.setScene(scene);
			            stage.show();
		
						stage.setOnCloseRequest(new EventHandler<WindowEvent>() 
						{
				            @Override
				            public void handle(WindowEvent event) {
				            	Photo ret = sc.onExit();
				                if (ret == null) {
				                	event.consume();
				                	stage.show();
				                } else {
				                	selectPhoto(ret);
				                };
				              
				            }
				        });
			            
			        }
			        catch (IOException e) {
			            e.printStackTrace();
			        }
			} else {
				AbstractController.showWarning("Slideshow Cancelled", "The action was discontinued.");
			}
		} else {
			AbstractController.showWarning("Slideshow Cancelled", "There are no photos in this album to display.");
		}
		
	}
	
	/**
	 * Switches back to the AlbumController after Confirming with User
	 * @param event ActionEvent to trigger the method
	 */
	public void goBack(ActionEvent event) {
		if(this.onExit() == true)
		{
			AlbumController al = new AlbumController();
			String fxml = "../view/AlbumPage.fxml";
	//		String[] args = {this.user.getName(),a.getName()};
			switchScene(this, al, this.s, fxml, this.userName);
		}
	}
	
	/**
	 * Clears all the fields in the Selection area of the window
	 */
	private void clearFields() {
		imageView.setImage(null);
		photoFileName.getChildren().clear();
		photoCaption.getChildren().clear();
		photoDateTime.getChildren().clear();
		if(photoTagList.getContent() instanceof FlowPane) {
			FlowPane f = (FlowPane) photoTagList.getContent();
			f.getChildren().clear();
		}
	}

	/**
	 * Fills the fields in the Selection area of the window with the attributes of the target Photo
	 * <ul>
	 * <li> image display </li>
	 * <li> filename </li>
	 * <li> caption </li>
	 * <li> modification Date </li>
	 * <li> tag list </li>
	 * </ul>
	 * @param p Photo to be selected
	 */
	private void fillPhotoFields(Photo p) {
		
		Text filename = new Text(p.getPath());
		Text caption = new Text(p.getCaption());
		Text date = new Text(p.getDateString());
		//System.out.println(p.getTags());
		this.clearFields();
		
		
		try {
			imageView.setImage(p.getImage());
			photoFileName.getChildren().add(filename);
			photoCaption.getChildren().add(caption);
			photoDateTime.getChildren().add(date);
			this.loadTags(p);
			
		} catch(Exception e) {
			AbstractController.showWarning("Content not loaded", "There was an error previewing "
					+ "the associated photo. It has been removed.");
			this.photoList.remove(p);
		}
	}
	
	

	/**
	 * Writes the modified Album to the userFile of the active User
	 */
	public void write() {
		try {
		
			ArrayList<Photo> list = (ArrayList<Photo>) this.photoList.stream().collect(Collectors.toList());
			this.album.updatePhotos(list);
			// write (individual) user to file
			FileOutputStream fos = new FileOutputStream(userLoadDir+File.separator+ this.user.getName()+".dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this.user);
			oos.close();

			
		} catch (Exception e) {
			showError(e);
		} 
		
	}
	
	/**
	 * Exits the application after Confirming with User
	 * @return boolean demonstrating if the User confirmed the exit or not
	 */
	@Override
	public boolean onExit() 
	{
		this.write();
		Optional<ButtonType> result = AbstractController.showConfirmation("Exit?", "Changes have been saved.");
		if(result.get() == ButtonType.OK) {
			return true;
		}
		else {
			return false;
		}
	}

}
