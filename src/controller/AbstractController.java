package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import app.Photos;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.User;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
/**
 * Abstract class which provides functionality to all main windows in the app
 * @author Ezra Ablaza
 * @author Kelvin Liu
 *
 */
public abstract class AbstractController 
{
	/**
	 * The scene title
	 */
	public String sceneTitle;
	
	/**
	 * Helper field to make it easier to access the user data directory
	 */
	public static final Path userLoadDir = 
			Paths.get(Photos.storeDir + Photos.individualUserDir + File.separator);

	
	/**
	 * Start method to be implemented by an extending Controller
	 * @param stage Stage to load the window on
	 * @param args Any applicable arguments to increase specificity 
	 * @throws Exception
	 */
	public abstract void start(Stage stage, String ...args) throws Exception;
	
	/**
	 * Abstract method to handle window exit behavior
	 * @return A boolean representing if exit was successful
	 */
	public abstract boolean onExit();
	
	/**
	 * Loads the User who corresponds to the specified username
	 * @param username A String containing the target username
	 * @return User object
	 * @throws Exception should a userFile not be available
	 */
	public User loadUserFile(String username) throws Exception {
		File userFile = new File(userLoadDir+File.separator+username+".dat");
		userFile.createNewFile();
		
		FileInputStream fileIn = new FileInputStream(userFile);
		if (fileIn.available() > 0) {
			ObjectInputStream in = new ObjectInputStream(fileIn);
			User user = (User) in.readObject();
			in.close();
			return user;
		}
		else {
			fileIn.close();
			throw new Exception("Specified userfile was not available.");
		}
	}
	
	protected Stage s;
	
	/**
	 * Implements the switch of Scenes, e.g. closing the current window and loading the next one
	 * @param prev the current Controller which will be exited
	 * @param next the next Controller to load
	 * @param stage the Stage upon which to set the next Scene
	 * @param fxml the fxml document which has the next Controller's layout
	 * @param args any additional arguments necessary
	 */
	public void switchScene(AbstractController prev, AbstractController next, Stage stage, String fxml, String... args)
	{
		try 
		{
			// on exit call
			//prev.onExit();
			
			//stage setup
			//load FXML
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			
			//controller setup
			loader.setController(next);
			
			//Parent
			Parent p  = (Parent) loader.load();
			
			//call start - load the song info
			next.start(stage, args);
			
			//set scene
			Scene scene = new Scene(p); 
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			stage.setScene(scene);
			stage.setTitle(next.sceneTitle);
			stage.setOnCloseRequest(new EventHandler<WindowEvent>() 
			{
	            @Override
	            public void handle(WindowEvent event) {
	                if (next.onExit() != true)
	                	{
	                		event.consume();
	                		//stage.show();
	                	};
	            }
	        });
			
			Photos.currentController = next;
			/*
			stage.setOnHidden(new EventHandler<WindowEvent>() 
			{
	            @Override
	            public void handle(WindowEvent event) 
	            {
	                next.onExit();
	            }
	        });
			*/
			//show
			stage.show();
		} 
		catch (Exception e) 
		{
			showError(e);
		}
	}
	
	/**
	 * Show Error message to Users and Devs with stack trace
	 * @param e Exception to be shown
	 */
	public static void showError(Exception e)
	{
		Alert alert = new Alert(AlertType.ERROR);
	
		alert.setTitle("Error");
		alert.setHeaderText(e.getMessage());
		//stack trace to dialog
		VBox dialogContent = new VBox();
		Label label = new Label("Stack Trace:");
		String stackTrace = getStackTrace(e);
		TextArea textArea = new TextArea();
		textArea.setText(stackTrace);
		dialogContent.getChildren().addAll(label, textArea);
		
		//set content
		alert.getDialogPane().setContent(dialogContent);
		alert.showAndWait();

		return;
	}
	/**
	 * Turn the stack trace of passed exception into a readable string format
	 * @param e Exception to be handled
	 * @return String of the stack trace
	 */
	private static String getStackTrace(Exception e) 
	{
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String s = sw.toString();
        return s;
	}
	
	/**
	 * Show Confirmation message to confirm action with User 
	 * @param title String containing Confirmation title
	 * @param content String containing further information
	 * @return Optional&lt;ButtonType&gt; object which confirms or cancels an action when resolved
	 */
	public static Optional<ButtonType> showConfirmation(String title, String content)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(title);
		alert.setContentText(content);
		
		return alert.showAndWait();	
		
	}

	/**
	 * Show Warning message to notify User of issue that is not an Error
	 * @param title String containing Warning title
	 * @param content String containing further information
	 */
	public static void showWarning(String title, String content)
	{
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(title);
		alert.setHeaderText("Warning:");
		alert.setContentText(content);
		alert.showAndWait();
	  
		return;
	}
	
	/**
	 * Show Alert message to notify User of important information
	 * @param a AlertType of message
	 * @param setTitle String containing Alert title
	 * @param setHeaderText String containing Alert header
	 * @param setContentText String containing further information
	 * @return Optional&lt;ButtonType&gt; object which confirms or cancels an action when resolved
	 */
	public Optional<ButtonType> showAlert
	(AlertType a, String setTitle, String setHeaderText, String setContentText) {
		// AlertType.ERROR
		Alert alert = new Alert(a);
		alert.setTitle(setTitle);
		alert.setHeaderText(setHeaderText);
		alert.setContentText(setContentText);
		return alert.showAndWait();
	}
	
	
}
