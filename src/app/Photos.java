package app;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import controller.AbstractController;
import controller.LoginController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.Parent;

/**
 * Represents the app 
 * @author Ezra Ablaza
 * @author Kelvin Liu
 *
 */
public class Photos extends Application {
	//data structures
	/*
	 * Keeps track of who is a registered user
	 */
	public static HashMap<String, Boolean> userMap;
	
	/**
	 * Keeps track of the current controller so that it can call its exitPage method on exit
	 */
	public static AbstractController currentController;
	
	
	
	
	//folders
	// data folder
	/**
	 * Folder in which all serialized data is stored
	 */
	public static final String storeDir = "." + File.separator + "dat";
	/**
	 * Folder which contains specific user information, namely
	 * the serialized users
	 */
	public static final String individualUserDir = File.separator + "Users";
	
	//files
	/**
	 * File of Users who are registered
	 */
	public static final String userFile =  File.separator + "Users.dat";
	/**
	 * File containing the userMap data
	 */
	public static final String userMapFile = "UsersMap.dat";
	
	// User map path
	/**
	 * String representing the path to the userMapFile
	 */
	public static final String userMapDirString = Photos.storeDir + File.separator + Photos.userMapFile;
	/**
	 * Path object representing the path to the userMapFile 
	 */
	public static final Path userMapDir = Paths.get(userMapDirString);
	
	/* (non-Javadoc) Starts the app
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 * @param primaryStage Stage object on which to set the initial window
	 */
	public void start(Stage primaryStage) 
	{
		//start with Login view
		try 
		{
			currentController = new LoginController();
			//stage setup
			//load fxml
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("../view/LoginPage.fxml"));
			loader.setController(currentController);
			//Parent
			Parent root = (Parent) loader.load();
			
			//call start
			currentController.start(primaryStage);
			
			//set scene
			Scene scene = new Scene(root);
			//CSS because why not -- actually never mind that
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.setTitle(currentController.sceneTitle);
			
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() 
			{
	            @Override
	            public void handle(WindowEvent event) {
	                if (currentController.onExit() != true)
	                	{
	                		event.consume();
	                		primaryStage.show();
	                	};
	            }
	        });
			
			
			//show
			primaryStage.show();
		} 
		catch(Exception e) 
		{
//			Alert alert = new Alert(AlertType.ERROR);
//			
//			alert.setTitle("Error");
//			alert.setHeaderText("Something went wrong:");
//			alert.setContentText(e.toString());
//
//			alert.showAndWait();
			AbstractController.showError(e);

		}	
	}
	

	/**
	 * The main method, which only launches the app
	 * @param args
	 */
	public static void main(String[] args) 
	{
		launch(args);
	}
	
}
