package model;

/**
 * Class to handle the Tag data structure
 * @author Ezra Ablaza
 *
 */
public class Tag implements java.io.Serializable {
	private static final long serialVersionUID = 6529685098267757691L;
	
	private String type;
	private String value;
	
	/**
	 * Returns an instance of a Tag with the type and value initialized to input arguments
	 * <ul>
	 * <li> The inputs are edited to remove whitespace and non-alphanumeric characters </li>
	 * </ul>
	 * @param t String new type
	 * @param v String new value
	 */
	public Tag(String t, String v) {
		t= t.toLowerCase().replaceAll("[[^a-zA-Z0-9]\\s+]", "");
		v = v.toLowerCase().replaceAll("[[^a-zA-Z0-9]\\s+]", "");
		this.type = t;
		this.value = v;
	}

	/**
	 * Retrieves the type of this Tag
	 * @return String this Tag's type
	 */
	public String getType() {
		return type;
	}
	/**
	 * Replaces the type of this Tag with the specified type
	 * @param type String representing the type to replace the current type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Retrieves the value of this Tag
	 * @return String this Tag's value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Replaces the value of this Tag with the specified value
	 * @param value String representing the value to replace the current value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Determines if this Tag is equal to another by comparing String representations
	 * @param o Object to check
	 * @return boolean reflecting if this Tag and o are similar 
	 */
	public boolean equals(Object o) {
		 return (o instanceof Tag && ((Tag) o).toString().equals(this.toString())); 
	}
	
	/**
	 * Converts this Tag to a String
	 * @return String representation of this Tag
	 */
	public String toString() {
		return this.type + "=" + this.value;
	}
}
