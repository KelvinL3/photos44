package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Class to handle Album data structure
 * @author Kelvin Liu
 *
 */
public class Album implements java.io.Serializable {
	private static final long serialVersionUID = 6529685098267757693L;
	
	private String name;
	private ArrayList<Photo> photos;
	private int numberOfPhotos;
	private Date earliest = null;
	private Date latest = null;
	
	String ownerName;
	
	/**
	 * Returns an instance of an Album containing
	 * <ul>
	 * <li> an album name </li>
	 * <li> an associated username </li>
	 * <li> an empty ArrayList&lt;Photo&gt; </li>
	 * <li> an int containing the number of stored Photos </li>
	 * <li> Date objects corresponding to the earliest and latest Photo entries </li>
	 * </ul>
	 * @param name String album name
	 * @param ownerName String associated username
	 */
	public Album(String name, String ownerName) {
		this.name = name;
		this.ownerName = ownerName;
		this.photos =  new ArrayList<Photo>();
		this.numberOfPhotos = 0;
//		this.earliest = null;
//		this.latest = null;
	}
	
	
	/**
	 * Returns an instance of an Album containing
	 * <ul>
	 * <li> an album name </li>
	 * <li> an associated username </li>
	 * <li> an ArrayList&lt;Photo&gt; initialized with the passed in Photos </li>
	 * <li> an int containing the number of stored Photos </li>
	 * <li> Date objects corresponding to the earliest and latest Photo entries </li>
	 * </ul>
	 * @param name String album name
	 * @param ownerName String associated username
	 * @param photos ArrayList&lt;Photo&gt; of the Photos to store
	 */
	public Album(String name, String ownerName, ArrayList<Photo> photos) {
		this(name, ownerName);
		this.photos = photos;
		if (photos==null || photos.isEmpty()) {
			return;
		}
		if (photos!=null) {
			Collections.sort(photos, (a,b)->a.getDate().compareTo(b.getDate()));
		}
		this.earliest = photos.get(0).getDate();
		this.latest = photos.get(photos.size()-1).getDate();
	}
	
	/**
	 * Retrieves the album name
	 * @return String album name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the album name
	 * @param name String new album name
	 */
	public void setName(String name) {
		this.name=name;
	}
	/**
	 * Retrieves the number of stored Photos
	 * @return int number of Photos
	 */
	public int getNumPhotos() {
		return numberOfPhotos;
	}
	/**
	 * Adds the specified Photo into the Album's ArrayList&lt;Photo&gt; and updates the 
	 * earliest and latest entry Dates
	 * @param p Photo to add
	 */
	public void addPhoto(Photo p) {
		this.photos.add(p);
		this.numberOfPhotos++;
		this.updateDates();
	}

	/**
	 * Removes the specified Photo from the Album's ArrayList&lt;Photo&gt; and updates the 
	 * earliest and latest entry Dates
	 * @param p Photo to delete
	 */
	public void removePhoto(Photo p) {
		this.photos.remove(p);
		this.numberOfPhotos--;
		this.updateDates();
	}
	
	/**
	 * Checks if the Album stores the specified Photo
	 * @param p Photo to check for
	 * @return boolean indicating if search was successful
	 */
	public boolean hasPhoto(Photo p) {
		return this.photos.indexOf(p) != -1;
	}
	
	/**
	 * Helper method to update the earliest and latest entry Dates of this Album
	 */
	public void updateDates() {
		
		if (this.photos.size() > 0) {
			Collections.sort(this.photos, (Photo p1, Photo p2) -> 
			p1.getDate().compareTo(p2.getDate()));
			this.earliest = this.photos.get(0).getDate();
			this.latest = this.photos.get(photos.size() - 1).getDate();
		} else {
			this.earliest = null;
			this.latest = null;
		}
	}
	
	/**
	 * Replaces the Album's list of Photos with the incoming ArrayList&lt;Photo&gt;
	 * @param list ArrayList&lt;Photo&gt; to replace the current list
	 */
	public void updatePhotos(ArrayList<Photo> list) {
		this.photos = list;
		this.numberOfPhotos = list.size();
		this.updateDates();
		return;
	}
	
	/**
	 * Retrieves the Album's list of Photos
	 * @return ArrayList&lt;Photo&gt; of this Album's Photos
	 */
	public ArrayList<Photo> getPhotos() {
		return this.photos;
	}
	
	/**
	 * Retrieves the earliest entry Date of this Album
	 * @return Date of earliest entry
	 */
	public Date getEarliest() {
		return earliest;
	}
	/**
	 * Retrieves the latest entry Date of this Album
	 * @return Date of latest entry
	 */
	public Date getlatest() {
		return latest;
	}
	
	
	/**
	 * Checks if this album is equal to another
	 * @param o Object to check
	 * @return boolean reflecting if this album and o are similar
	 */
	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (o==this)
			return true;
		if (!(o instanceof Album)) return false;
		Album a = (Album) o;
		if (a.name == null)
			return false;
			//System.out.println("Error 1");
		if (a.ownerName == null)
			return false;
			//System.out.println("Error 2");
	    return a.name.equals(this.name) && a.ownerName.equals(this.ownerName);
	}
	
	
	/**
	 * Converts this Album to a String
	 * @return String representation of this Album
	 */
	@Override
	public String toString() {
		int size = photos == null ? 0 : photos.size();
//		String spaces = "";
//		System.out.println((this.name.length()+spaces.length()));
//		while ((this.name.length()+spaces.length())<=this.padding) {
//			System.out.println((this.name.length()+spaces.length()));
//			spaces = spaces+" ";
//		}
		return this.name + "\t\t\t" + size + "\t\t" + earliest + "\t\t" + latest;
	}
}
