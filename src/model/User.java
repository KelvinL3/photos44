package model;

import java.util.ArrayList;

import controller.AbstractController;
/**
 * Class to handle the User data structure
 * @author Kelvin Liu
 *
 */
public class User implements java.io.Serializable {
	private static final long serialVersionUID = 6529685098267757690L;
	
	private String name;
	private ArrayList<Album> albums;
	boolean isAdmin = false;
	
	/**
	 * Returns an instance of a User with 
	 * <ul>
	 * <li> a String username </li>
	 * <li> an ArrayList&lt;Album&gt; corresponding to the User's owned Albums </li>
	 * <li> a boolean reflecting if the User is an Admin or not </li>
	 * </ul>
	 * @param userName String to specify username
	 */
	public User(String userName) {
		this.name = userName;
		this.albums = new ArrayList<Album>();
	}
	/**
	 * Returns an instance of a User with 
	 * <ul>
	 * <li> a String username </li>
	 * <li> an ArrayList&lt;Album&gt; corresponding to the User's owned Albums </li>
	 * <li> a boolean reflecting if the User is an Admin or not </li>
	 * </ul>
	 * @param userName String to specify username
	 * @param albums ArrayList&lt;Album&gt; to specify Albums owned
	 */
	public User(String userName, ArrayList<Album> albums) {
		this(userName);
		this.albums = albums;
	}
	/**
	 * Returns an instance of a User with 
	 * <ul>
	 * <li> a String username </li>
	 * <li> an ArrayList&lt;Album&gt; corresponding to the User's owned Albums </li>
	 * <li> a boolean reflecting if the User is an Admin or not </li>
	 * </ul>
	 * @param userName String to specify username
	 * @param albums ArrayList&lt;Album&gt; to specify Albums owned
	 * @param isAdmin boolean to determine Admin status
	 */
	public User(String userName, ArrayList<Album> albums, boolean isAdmin) {
		this(userName, albums);
		this.isAdmin = isAdmin;
	}
	
	/**
	 * Retrieves the list of Albums owned by this User
	 * @return ArrayList&lt;Album&gt; containing owned Albums
	 */
	public ArrayList<Album> getAlbums() {
		return albums;
	}
	
	/**
	 * Retrieves a specific Album from this User by its specified name
	 * <ul>
	 * <li> Returns null if Album is not found </li>
	 * </ul>
	 * @param name String album name
	 * @return Album with the name specified
	 * @throws Exception
	 */
	public Album getAlbum (String name) throws Exception
	{
		for(Album a : this.albums)
		{
			if(a.getName().equals(name))
			{
				return a;
			}
		}
		throw new Exception("Album not found!!!!!.");
	}
	
	/**
	 * Adds specified Album to this User's list of albums
	 * @param a Album to add
	 */
	public void addAlbum(Album a) {
		albums.add(a);
	}
	/**
	 * Removes specified Album from this User's list of albums
	 * @param a Album to remove
	 * @return boolean reflecting if the ArrayList&lt;Album&gt; list contained a
	 */
	public boolean removeAlbum(Album a) {
		return albums.remove(a);
	}
	
	/**
	 * Retrieves the name of this User
	 * @return String username
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Converts this User to a String
	 * @return String representation of this User
	 */
	@Override
	public String toString(){
	    return (this.name + ":\t\t Number of Albums: " + albums.size());
	}
	
}
