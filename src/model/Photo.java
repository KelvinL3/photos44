
package model;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import controller.AbstractController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

/**
 * Class to handle Photo data structure
 * @author Ezra Ablaza
 *
 */
public class Photo implements java.io.Serializable {
	private static final long serialVersionUID = 6529685098267757692L;
	
	private String path;
	private ArrayList<Tag> tags;
	private String caption;
	private Date date;


	
	/**
	 * Returns an instance of a Photo containing
	 * <ul>
	 * <li> a String representation of the image path </li>
	 * <li> a String caption </li>
	 * <li> an empty ArrayList&lt;Tag&gt; </li>
	 * <li> a Date object corresponding to image modification date </li>
	 * </ul>
	 * @param path Path object that contains the image
	 * @param album String album name to which the Photo belongs
	 */
	public Photo(Path path, String album) {
		try {
			this.path = path.toString(); // p.toString();
		
			this.tags = new ArrayList<Tag>();
			
			BasicFileAttributes attrs = Files.readAttributes(path, BasicFileAttributes.class);
			FileTime time = attrs.lastAccessTime();
			this.date = new Date(time.toMillis());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			AbstractController.showError(e);
		}
		//System.out.println(this.date);
	}
	
	
	/**
	 * Retrieves the path of the Photo
	 * @return String this Photo's path
	 */
	public String getPath() {
		return "file:" + System.getProperty("user.dir") + File.separator + this.path;
	}
	
	/**
	 * Retrieves the Image referred to by this Photo
	 * @return Image referenced by Photo path
	 * @throws Exception
	 */
	public Image getImage() throws Exception{
        Image image;
		image = new Image(this.getPath());
        return image;

	}
	
	/**
	 * Retrieves a shortened version of the Photo's path
	 * @return String this Photo's shortened path
	 */
	public String getShortPath() {
		return this.path.substring(this.path.lastIndexOf(File.separator) + 1, this.path.length());
	}
	
	/**
	 * Retrieves the Tags associated with this Photo
	 * @return ArrayList&lt;Tag&gt; the list of this Photo's tags
	 */
	public ArrayList<Tag> getTags() {
		return this.tags;
	}
	
	/**
	 * Retrieves the Tags associated with this Photo whose types match the one specified
	 * @param t String specifying Tag type
	 * @return ArrayList&lt;Tag&gt; the Tags of this Photo which are eligible
	 */
	public ArrayList<Tag> getTagsByType(String t) {
		 return (ArrayList<Tag>) this.tags.stream().filter(tag -> 
		 tag.getType().equals(t)).collect(Collectors.toList());
	}
	
	/**
	 * Retrieves the Tags associated with this Photo whose values match the one specified
	 * @param v String specifying Tag value
	 * @return ArrayList&lt;Tag&gt; the Tags of this Photo which are eligible
	 */
	public ArrayList<Tag> getTagsByValue(String v) {
		 return (ArrayList<Tag>) this.tags.stream().filter(tag -> 
		 tag.getValue().equals(v)).collect(Collectors.toList());
	}
	
	/**
	 * Adds the specified Tag to this Photo's list
	 * @param t Tag to add
	 */
	public void addTag(Tag t) {
		this.tags.add(t);
	}
	
	/**
	 * Removes the specified Tag from this Photo's list
	 * @param t Tag to remove
	 */
	public void removeTag(Tag t) {
		this.tags.remove(t);
	}
	
	/**
	 * Retrieves the caption for this Photo
	 * @return String Photo caption
	 */
	public String getCaption() {
		return this.caption;
	}
	

	/**
	 * Sets the caption for this photo to the String specified
	 * @param c String caption to replace the current caption
	 */
	public void changeCaption(String c) {
		this.caption = c;
	}
	
	/**
	 * Retrieves the Date object corresponding to this Photo's modification Date
	 * @return Date Photo's modification date 
	 */
	public Date getDate() {
		return this.date;
	}
	
	/**
	 * Retrieves this Photo's modification Date as a String
	 * @return String representation of this Photos' modification date
	 */
	public String getDateString() {
		return this.date.toString();
	}
	

	/**
	 * Determines if this Photo is equal to another by comparing paths
	 * @param o Object to check
	 * @return boolean reflecting if this Photo and o are similar 
	 */
	public boolean equals(Object o) {
		 return (o instanceof Photo && ((Photo) o).getPath().equals(this.getPath())); 
	}
}
