
package model;

import java.util.ArrayList;
import java.util.List;
/**
 * Container class for search operation conducted in AlbumController
 * @author Kelvin Liu
 *
 */
public class Query {
	/**
	 * List containing the results of this Query
	 */
	public List<Tag> ans = new ArrayList<Tag>();
	/**
	 * List containing the conjunctions between this Query and others
	 */
	public List<String> andOr = new ArrayList<String>();
	
	/**
	 * Returns an instance of a Query with empty ans and andOr Lists
	 */
	public Query() {}
	
	/**
	 * Converts this Query to a String
	 * @return String representation of this Query
	 */
	public String toString() {
		String temp = "";
		for (Tag t : ans){
			temp = temp + t + " ";
		}
		for (String s : andOr){
			temp = temp + s + " ";
		}
		return temp;
	}
}
